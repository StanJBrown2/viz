#include "viz/camera.hpp"

Camera::Camera(const glm::vec3 position) : position{position} {
  cameraUpdate(*this);
}

Camera::Camera(const glm::vec3 position, const glm::vec3 up, const float yaw,
               const float pitch)
    : position{position}, up{up}, yaw{yaw}, pitch{pitch} {
  cameraUpdate(*this);
}

Camera::Camera(const float pos_x, const float pos_y, const float pos_z,
               const float up_x, const float up_y, const float up_z,
               const float yaw, const float pitch)
    : position{pos_x, pos_y, pos_z},
      world_up{up_x, up_y, up_z},
      yaw{yaw},
      pitch{pitch} {
  cameraUpdate(*this);
}

glm::mat4 cameraViewMatrix(const Camera &camera) {
  return glm::lookAt(camera.position, camera.position + camera.front,
                     camera.up);
}

void cameraUpdate(Camera& camera) {
  glm::vec3 front;

  front.x = cos(glm::radians(camera.yaw)) * cos(glm::radians(camera.pitch));
  front.y = sin(glm::radians(camera.pitch));
  front.z = sin(glm::radians(camera.yaw)) * cos(glm::radians(camera.pitch));

  camera.front = glm::normalize(front);

  //  Also re-calculate the camera.right and up vector
  camera.right = glm::normalize(glm::cross(
      camera.front,
      camera.world_up));  // Normalize the vectors, because their length gets
                          // closer to 0 the more you look up or down which
                          // results in slower movement.
                          //
  camera.up = glm::normalize(glm::cross(camera.right, camera.front));
}

void cameraKeyboardHandler(Camera &camera, const CameraMovement &direction,
                           const float dt) {
  float velocity = camera.movement_speed * dt;
  if (direction == FORWARD) {
    camera.position += camera.front * velocity;
  }
  if (direction == BACKWARD) {
    camera.position -= camera.front * velocity;
  }
  if (direction == LEFT) {
    camera.position -= camera.right * velocity;
  }
  if (direction == RIGHT) {
    camera.position += camera.right * velocity;
  }
}

void cameraMouseHandler(Camera &camera, const float xoffset,
                        const float yoffset, const GLboolean constrain_pitch) {
  camera.yaw += xoffset * camera.mouse_sensitivity;
  camera.pitch += yoffset * camera.mouse_sensitivity;

  // Make sure that when pitch is out of bounds, screen doesn't get flipped
  if (constrain_pitch) {
    if (camera.pitch > 89.0f) {
      camera.pitch = 89.0f;
    }
    if (camera.pitch < -89.0f) {
      camera.pitch = -89.0f;
    }
  }

  // Update camera.front, camera.right and Up Vectors using the updated Euler
  // angles
  cameraUpdate(camera);
}

void cameraScrollHandler(Camera &camera, const float offset_y) {
  camera.position += camera.front * offset_y / 2.0f;
}

void cameraPanHandler(Camera &camera, float offset_x, float offset_y,
                      float pan_speed) {
  camera.position -= camera.right * offset_x / pan_speed;
  camera.position -= camera.up * offset_y / pan_speed;
}
