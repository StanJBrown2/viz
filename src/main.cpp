#include <glad/glad.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

#include "shader.hpp"
#include "viz/camera.hpp"
// resource manager
// data
// shader classes

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// setup our camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float last_x = SCR_WIDTH / 2.0f;
float last_Y = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// some stuff or monitoring frame rate
// timing
float dt = 0.0f;  // time between current frame and last frame
float last_frame = 0.0f;

// callbacks
void framebufferSizeCallback(GLFWwindow* window, int width, int height);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void processKeyboardInput(GLFWwindow* window);

void drawPoints(Shader& shader, float x, float y, float z) {
  float vertices[] = {
      10.5f,  -10.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,  // bottom right
      -10.5f, -10.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // bottom left
      10.0f,  10.5f,  0.0f, 0.0f, 0.0f, 1.0f, 1.0f   // top
  };

  unsigned int VBO;
  unsigned int VAO;

  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // position attribute (vec3)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // color attribute (rgbi) vec4
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                        (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  shader.use();
  glBindVertexArray(VBO);
  // glPointSize(5.0f);
  glDrawArrays(GL_TRIANGLES, 0, 3);
}

int main() {
  // glfw: initialize and configure
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
// uncomment this statement to fix compilation on OS X
#endif

  // glfw window creation
  GLFWwindow* window =
      glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Learning", NULL, NULL);
  if (window == NULL) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }

  // look at this at some point to make multiple windows
  glfwMakeContextCurrent(window);

  // bind the callbacks
  glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
  glfwSetCursorPosCallback(window, mouseCallback);
  glfwSetScrollCallback(window, scrollCallback);

  // tell GLFW to capture the mouse, but leave the cursor alone
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

  // glad: load all OpenGL function pointers
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }

  glEnable(GL_DEPTH_TEST);  // allows for cliping and view point checking

  // build my own shader
  Shader colorShader(
      "/home/stan/projects/labforge/viz/include/shaders/color_shader.vs",
      "/home/stan/projects/labforge/viz/include/shaders/color_shader.fs");

  // render loop
  while (!glfwWindowShouldClose(window)) {
    // per-frame time logic
    float currentFrame = glfwGetTime();
    dt = currentFrame - last_frame;
    last_frame = currentFrame;

    // input
    processKeyboardInput(window);

    // render
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // pass projection matrix to shader
    glm::mat4 projection =
        glm::perspective(glm::radians(camera.zoom),
                         (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

    // camera/view transformation
    glm::mat4 view = cameraViewMatrix(camera);
    glm::mat4 T_origin_model = glm::mat4(1.0f);
    // T_origin_model = glm::rotate(T_origin_model, (float)glfwGetTime(),
    //                              glm::vec3(0.5f, 1.0f, 0.0f));

    colorShader.setMat4("projection", projection);
    colorShader.setMat4("view", view);
    colorShader.setMat4("T_origin_model", T_origin_model);
    drawPoints(colorShader, 0, 0, 3.0f);

    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved
    // etc.)
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // glfw: terminate, clearing all previously allocated GLFW resources.
  glfwTerminate();
  return 0;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
  // make sure the viewport matches the new window dimensions;
  glViewport(0, 0, width, height);
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
  if (firstMouse) {
    last_x = xpos;
    last_Y = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - last_x;
  // reversed since y-coordinates go from bottom to top
  float yoffset = last_Y - ypos;

  last_x = xpos;
  last_Y = ypos;

  int right_button_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
  int left_button_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);

  if (right_button_state == GLFW_PRESS) {
    cameraPanHandler(camera, xoffset, yoffset);
  }

  if (left_button_state == GLFW_PRESS) {
    cameraMouseHandler(camera, xoffset, yoffset);
  }
  // todo add better panning if you hold down both buttons, (fast panning)
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
  cameraScrollHandler(camera, yoffset);
}

void processKeyboardInput(GLFWwindow* window) {
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, true);
  }

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    cameraKeyboardHandler(camera, FORWARD, dt);
  }

  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    cameraKeyboardHandler(camera, BACKWARD, dt);
  }

  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    cameraKeyboardHandler(camera, LEFT, dt);
  }

  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    cameraKeyboardHandler(camera, RIGHT, dt);
  }
}
