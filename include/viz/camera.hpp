#ifndef __VIZ__CAMERA_H__
#define __VIZ__CAMERA_H__

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum CameraMovement { FORWARD, BACKWARD, LEFT, RIGHT };

struct Camera {
  // state
  glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
  glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

  glm::vec3 right;
  glm::vec3 world_up = up;

  float yaw = -90.0f;
  float pitch = 0.0f;

  // Settings
  float movement_speed = 10.5f;
  float mouse_sensitivity = 0.1f;
  float zoom = 45.0f;  // affects the fov

  Camera(const glm::vec3 position);
  Camera(const glm::vec3 position, const glm::vec3 up, const float yaw,
         const float pitch);

  Camera(const float pos_x, const float pos_y, const float pos_z,
         const float up_x, const float up_y, const float up_z, const float yaw,
         const float pitch);
};

glm::mat4 cameraViewMatrix(const Camera &camera);

void cameraUpdate(Camera &camera);

void cameraKeyboardHandler(Camera &camera, const CameraMovement &direction,
                           const float dt);

void cameraMouseHandler(Camera &camera, const float xoffset,
                        const float yoffset,
                        const GLboolean constrainPitch = true);

void cameraScrollHandler(Camera &camera, const float offset_y);

void cameraPanHandler(Camera &camera, float offset_x, float offset_y,
                      float pan_speed = 10.0f);

#endif
