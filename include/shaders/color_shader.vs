#version 330 core

layout (location = 0) in vec3 model_position;
layout (location = 1) in vec4 color;

out vec4 color_from_vshader;

uniform mat4 T_origin_model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view * T_origin_model * vec4(model_position, 1.0f);
	// color_from_vshader = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	color_from_vshader = color;
}
